package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(produces= {"application/json"})
@Slf4j
public class Controller {
	
	@GetMapping
	public Business persona() {
			Business business = new Business();
			business.setNombre("Andres");
			business.setTexto("Esto es un Get");
			return business;
	}
	
	@PostMapping(produces ="application/json")
	public Business hola1 (@ResquestBody Dao dao) {
		Business business = new Business();
		business.setNombre("Andres");
		business.setTexto("Esto es un post");
		return business;
		
	}
	
@DeleteMapping("business")
@ResponseStatus(code = HttpStatus.NO_CONTENT)
public void borraBusiness() {
	log.info("request delete recibida,borra algo");
	
}
	
}
